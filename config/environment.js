/* eslint-env node */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'facturacion',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    moment: {
      includeLocales: ['es']
    },

    'ember-load': {
      // This is the default value, if you don't set this opton
      loadingIndicatorClass: 'ember-load-indicator'
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    fastboot: {
      hostWhitelist: ['appspot.com', 'firebaseio.com', 'firebaseapp.com', /^localhost:\d+$/]
    },
    torii: {
      sessionServiceName: 'session'
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV['firebase'] = {
      apiKey: "AIzaSyDX2jNCmY4h9vS6IuW4HIEFAqKoQwC8__w",
      authDomain: "contamc-facturacion-dev.firebaseapp.com",
      databaseURL: "https://contamc-facturacion-dev.firebaseio.com",
      projectId: "contamc-facturacion-dev",
      storageBucket: "contamc-facturacion-dev.appspot.com",
      messagingSenderId: "428360454597",
      deployToken: "1/9-7SBaHgM4Gg6wkEJDI1d0VvYSv2hCKRFzXmq3T69UQ_kaJZpO_hP_D6ekSD-yVb"
    };

    ENV['elasticsearch'] = {
      host: [{
        host: 'elastic.contamc.com',
        auth: 'ember:emberclient',
        protocol: 'https',
        port: 443
      }]
    };

    ENV['api'] = {
      host: 'https://localhost:4001'//api-dev.contamc.com'
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';

    ENV['elasticsearch'] = {
      host: [{
        host: 'elastic.contamc.com',
        auth: 'ember:emberclient',
        //host: 'elastic.dev.apprendia.tech',
        //auth: 'elastic:changeme',
        protocol: 'https',
        port: 443
      }]
    }
  }

  if (environment === 'production') {
    ENV['firebase'] = {
      apiKey: "AIzaSyCG9PI2LYtmn2QdHrr63En9HVHWJdetuCo",
      authDomain: "contamc-facturacion.firebaseapp.com",
      databaseURL: "https://contamc-facturacion.firebaseio.com",
      projectId: "contamc-facturacion",
      storageBucket: "contamc-facturacion.appspot.com",
      messagingSenderId: "1072920833228",
      deployToken: '1/Z4Zcp-eGYONE0rob04byj_xqV-M881x6RKTTMVmAzXQ'
    };

    ENV['elasticsearch'] = {
      host: [{
        host: 'elastic.contamc.com',
        auth: 'ember:emberclient',
        protocol: 'https',
        port: 443
      }]
    };

    ENV['api'] = {
      host: 'https://api.contamc.com'
    }
  }

  return ENV;
};
