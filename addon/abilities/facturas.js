import { computed } from '@ember/object';
import { readOnly } from '@ember/object/computed';
import {inject as service} from '@ember/service';
import { Ability } from 'ember-can';

export default Ability.extend({
	currentUser: service(),
	user: readOnly('currentUser.model'),

	canButtonCreate: computed('user.isSaeko',function(){
		return !this.get('user.isSaeko');
	})
});
