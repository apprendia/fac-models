import { computed } from '@ember/object';
import { equal, alias } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import DS from 'ember-data';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/impuesto';
import Decimal from 'npm:decimal.js-light';
import { inject as service } from '@ember/service';

import {
  RETENCION,
  TRASLADADO,
  FACTOR_CUOTA,
  FACTOR_TASA
} from '../utils/catalogos/impuestos';



export const Flags = Mixin.create({
  isRetenido: equal('tipo', RETENCION),
  isTrasladado: equal('tipo', TRASLADADO),
  isCuota: equal('tipoFactor', FACTOR_CUOTA),
  isTasa: equal('tipoFactor', FACTOR_TASA),
  isLocal: alias('local'),
  isExento: equal('tipoFactor', "Exento")
});

export const ComputedProps = Mixin.create({
  importe: computed('base', 'base.content', 'tasaCuota', 'tipoFactor', 'isLocal', 'precision.decimals',{
    get() {
      if(this.get('isExento')) return null
        debugger
      switch (true) {
        case !!this.get('importeCustom'):
          return Decimal(this.get('importeCustom')).todp(this.get('precision.decimals')).toNumber();
        case this.get('isCuota'):
          return Decimal(this.get('tasaCuota') || 0).todp(this.get('precision.decimals')).toNumber();
        case this.get('isTasa'):
          if(this.get('isGlobal')&&(this.get('impuesto')=="002"||this.get('impuesto')=="003")){
            let factura = this.store.peekRecord('factura', this.get('facturaId'))
            let total  = 0
            factura.get('conceptos').forEach((concepto)=>{
              let importe = concepto.get('importe')
              let importeForSuma = Decimal(importe || 0).todp(this.get('precision.decimals')).mul(this.get('tasaCuota') || 0).todp(this.get('precision.decimals')).div(100).todp(this.get('precision.decimals')).toNumber();
              total = Decimal(total).todp(this.get('precision.decimals')).add(importeForSuma).todp(this.get('precision.decimals')).toNumber()
            })
            return total
            // debugger
            // return factura.get('conceptos').then((conceptos)=>{
            //   conceptos.forEach((concepto)=>{
            //     let importe = concepto.get('importe')
            //     let importeForSuma = Decimal(importe || 0).todp(this.get('precision.decimals')).mul(this.get('tasaCuota') || 0).todp(this.get('precision.decimals')).div(100).todp(this.get('precision.decimals')).toNumber();
            //     total = Decimal(total).todp(this.get('precision.decimals')).add(importeForSuma).todp(this.get('precision.decimals')).toNumber()
            //   })
            // }).then(()=>{
            //   console.log(total)
            //   return total
            // })
          }
          return Decimal(this.get('base.content') || this.get('base') || 0).todp(this.get('precision.decimals')).mul(this.get('tasaCuota') || 0).todp(this.get('precision.decimals')).div(100).todp(this.get('precision.decimals')).toNumber();
      }
    },

    set(_, value) {
      this.set('importeCustom', value);
      return Decimal(value).todp(6).toNumber(this.get('precision.decimals'));
    }
  }).meta({ serialize: true }),

});

export default DS.Model.extend(Copyable, Validations, Flags, ComputedProps, {
  precision: service('round-precision'),

  tipo: DS.attr("number", {
    defaultValue: 1
  }),
  /*Se llena del sistema 1 o 2 retenido o trasladado*/
  base: DS.attr("number"),
  impuesto: DS.attr("string"),
  tasaCuota: DS.attr("number"),
  local: DS.attr('boolean', { defaultValue: false }),
  tipoFactor: DS.attr("string", {
    defaultValue: 'Tasa'
  }),
  isGlobal: DS.attr('boolean', { defaultValue: false }),
  facturaId: DS.attr('string'),

  importeForSuma: computed('importe', 'tipo', 'precision.decimals', function () {
    let { isRetenido, importe } = this.getProperties('isRetenido', 'importe');
    if(importe)
      return isRetenido ? Decimal(importe).negated().todp(this.get('precision.decimals')).toNumber() : Decimal(importe).todp(this.get('precision.decimals')).toNumber();
    else
      return 0
  }),
  total: alias('importeForSuma'),

  tasaCuotaStr: computed('tasaCuota', {
    get() {
      return `${this.get('tasaCuota')}`
    },
    set(_, value) {
      this.set('tasaCuota', Decimal(value).todp(4).toNumber());
      return value;
    }
  })
});
