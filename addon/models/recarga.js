import DS from 'ember-data';
import moment from 'moment';
import { computed, get } from '@ember/object';
import { isBlank } from '@ember/utils';



export default DS.Model.extend({
	pago: DS.attr('number'),
	creditos: DS.attr('number'),
	fecha: DS.attr("string"),
	fechaUnix: computed('fecha', function () {
		return (!isBlank(this.get('fecha'))) ? moment.utc(this.get('fecha')).unix() : 0;
	}).meta({ serialize: true }),
	facturas: DS.hasMany('factura'),
	activo: DS.attr('boolean'),
	facturasCount: DS.attr('number'),
});
