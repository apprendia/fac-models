import DS from 'ember-data';

export default DS.Model.extend({
	code: DS.attr('string'),
	label: DS.attr('string'),
	logo: DS.attr('string')
});
