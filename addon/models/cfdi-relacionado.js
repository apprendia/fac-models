import { observer } from '@ember/object';
import Copyable from 'ember-data-copyable';
import DS from 'ember-data';
import {
	buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({


});


export default DS.Model.extend(Copyable, Validations, {

	uuid: DS.attr("string"),
	tipoRelacion: DS.attr("string"),
	factura: DS.belongsTo("factura"),

	etiquetas: DS.attr(),

	etiqueta: observer('uuid', function() {
		let comaEncontrada = false;
		if (String(this.get('uuid')).indexOf(',') != -1) {
			let et = this.get('uuid').replace(/[,]+/g, '');
			comaEncontrada = true;
			if (this.get('etiquetas')) {
				let posicion = this.get('etiquetas').length;
				this.get('etiquetas').insertAt(posicion, et);
				this.set('uuid', undefined);
			} else {
				this.set('etiquetas', [et]);
				this.set('uuid', undefined);
			}

			comaEncontrada = false;
		} else {
			comaEncontrada = false;
		}

		return comaEncontrada;
	}),



});
