import { all } from 'rsvp';
import DS from 'ember-data';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/concepto';
import Calculations from 'ember-cli-facturacion-logic/mixins/calculations/concepto';
import SelectionFields from 'ember-cli-facturacion-logic/mixins/selected-objects/concepto';

export default DS.Model.extend(Validations, Copyable, Calculations, SelectionFields, {
  validateCantidad: true,
  validateImporte: true,

  /**
   * @override: ember lifecycle
   */
  init(...params) {
    this._super(...params);
    this.set('copyableOptions',{});
  },


  clave: DS.attr("string"), //ClaveProdServ
  noIdentificacion: DS.attr("string"),
  cantidad: DS.attr("number", {defaultValue: 1}),
  claveUnidad: DS.attr("string"),
  unidad: DS.attr("string"),
  descripcion: DS.attr("string"),
  valorUnitario: DS.attr("number"),
  descuento: DS.attr("number",{ defaultValue: 0 }),
  numeroPredial: DS.attr("string"),
  isCatalogo: DS.attr("boolean", { defaultValue: false }),

  isParte: DS.attr("boolean", {
    defaultValue: false
  }),

  impuestos: DS.hasMany("impuesto", {
    inverse: null,
    // cascadeDelete: true
  }),

  partes: DS.hasMany('concepto', {
    inverse: null,
    // cascadeDelete: true
  }),

  // Complementos
  iedu: DS.belongsTo("complementoConceptos/iedu", {
    // cascadeDelete: true
  }),

  fullSave() {
    let promises = [this.save()];

    if (this.hasMany('impuestos').value()) {
      promises.pushObject(all(this.hasMany('impuestos').value().invoke('save')));
    }

    if (this.hasMany('partes').value()) {
      promises.pushObject(all(this.hasMany('partes').value().invoke('save')));
    }

    if (this.belongsTo('iedu').value()) {
      promises.pushObject(this.belongsTo('iedu').value().save());
    }

    return all(promises);
  },


});
