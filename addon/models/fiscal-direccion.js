import DS from 'ember-data';
import {
	validator,
	buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
	direccion: validator('presence', true),
	cp: validator('presence', true)
});

export default DS.Model.extend(Validations, {
	nombre: DS.attr('string', {
		defaultValue: 'Matriz'
	}),
	direccion: DS.attr("string"),
	cp: DS.attr("string"),
	// municipioID: DS.attr("number"),
	// estadoID: DS.attr("number"),
	_destroy: DS.attr("boolean", {
		defaultValue: false
	})
});
