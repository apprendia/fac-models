import DS from 'ember-data';

export default DS.Model.extend({
	account: DS.attr('string'),
	factura: DS.belongsTo('factura'),
	error: DS.attr('string'),
	status: DS.attr('number')
});
