import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
	serie: DS.attr('string'),
	lastFolio: DS.attr('number', { defaultValue: 0 }),
	nextFolio: computed('lastFolio', function () {
		return Number(this.get('lastFolio') || 0) + 1;
	})
});
