import DS from 'ember-data';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/complemento/ine/contabilidad';

export default DS.Model.extend(Validations, Copyable, {
	copyableOptions: {},

	idContabilidad: DS.attr('string')
});
