import DS from 'ember-data';
import {all} from 'rsvp';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/complemento/ine';
import {filterBy} from '@ember/object/computed';

export default DS.Model.extend(Validations, Copyable, {
  copyableOptions: {},

  version: DS.attr('string', { defaultValue: '1.1' }),
  tipoProceso: DS.attr('string'),
  tipoComite: DS.attr('string'),
  idContabilidad: DS.attr('string'),

  entidades: DS.hasMany('complemento/ine/entidad', { cascadeDelete: true }),
  entidadesLive: filterBy('entidades', 'isDeleted', false),


  fullSave() {
    let promises = [this.save()];

    promises.pushObject(
      this.get('entidades').then(function(entidades){
        return all(entidades.invoke('fullSave'))
      })
    )

    return all(promises);
  }
});
