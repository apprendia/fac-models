import DS from 'ember-data';
import {
	validator,
	buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
	autorizacion: validator('presence'),
	fecha: validator('presence'),
	fiscalEmisor: validator('presence')
});

export default DS.Model.extend(Validations, {
	autorizacion: DS.attr("string"),
	fecha: DS.attr("string"),
	fiscalEmisor: DS.belongsTo("fiscalEmisor")
});
