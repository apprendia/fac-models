import { computed } from '@ember/object';
import DS from 'ember-data';
import moment from 'moment';
import {
	validator,
	buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
	fiscalEmisor: validator('presence', true),
	rfc: [
		validator('presence', true),
		validator('confirmation', {
			on: 'fiscalEmisor.rfc',
			dependentKeys: ['model.fiscalEmisor.rfc'],
		})
	],
	numero: validator('presence', true),
	pemCer: [
		validator('presence', true),
		validator('cert-key-related', {
			dependentKeys: ['model.pemCer', 'model.pemKey', 'pemCer', 'pemKey'],
			isWarning: true
		})
	],
	pemKey: [
		validator('presence', true),
		validator('cert-key-related', {
			dependentKeys: ['model.pemCer', 'model.pemKey', 'pemCer', 'pemKey'],
			isWarning: true
		})
	],
	pemKeySecure: validator('presence', true),
	notAfter: [
		validator('presence', true),
		validator('date', {
			after: 'now',
			precision: 'second',
			message: 'El certificado (CSD) ha caducado. Se debe generar uno nuevo desde el portal del SAT'
		})
	],
	notBefore: [
		validator('presence', true),
		validator('date', {
			before: 'now',
			precision: 'second',
			message: 'Aún no se puede ocupar el el certificado (CSD) para facturar'
		})
	],

	certKeyRelated: validator('cert-key-related', {
		dependentKeys: ['model.pemCer', 'model.pemKey', 'pemCer', 'pemKey'],
		isWarning: true
	})
});

export default DS.Model.extend(Validations, {
	numero: DS.attr("string"),
	status: DS.attr("number"),
	testOnly: DS.attr("boolean"),
	pemCer: DS.attr("string"),
	pemKey: DS.attr("string"),
	pemKeySecure: DS.attr("string"),
	notAfter: DS.attr("date"),
	notBefore: DS.attr("date"),
	rfc: DS.attr("string"),
	//users: DS.belongsTo("user"),
	fiscalEmisor: DS.belongsTo("fiscalEmisor"),
	_destroy: DS.attr("boolean", {
		defaultValue: false
	}),
	isUsable: computed('notAfter', 'notBefore', function() {
		return moment().isBetween(this.get('notBefore'), this.get('notAfter'));
	})
});
