import DS from 'ember-data';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/iedu';
import {empty} from '@ember/object/computed';

export default DS.Model.extend(Copyable, Validations,{


	/**
	 * @override: ember lifecycle
	 */
	init(...params) {
		this._super(...params);
		this.set('copyableOptions', {});
	},

	nombreAlumno: DS.attr('string'),
	curp: DS.attr('string'),
	nivelEducativo: DS.attr('string'),
	autRVOE: DS.attr('string'),

	rvoeEmpty: empty('autRVOE')
});
