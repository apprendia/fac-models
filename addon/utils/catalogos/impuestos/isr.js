const id = '001';
const obj = {
  id: id,
  label: 'ISR',
  inputTypes: ['range'],
  range: {
    min: 0.000000,
    max: 0.350000
  },
  trasladado: false,
  retencion: true
};
export {
  id as
  default, obj
}
