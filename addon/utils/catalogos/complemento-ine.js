import { set } from '@ember/object';
import { copy } from '@ember/object/internals';
import { merge } from '@ember/polyfills';

export const PRECAMPANA = '1';
export const ORDINARIO = '2';
export const CAMPANA = '3';
export const TIPPROCESO = {};

TIPPROCESO[PRECAMPANA] = 'Precampaña';
TIPPROCESO[ORDINARIO] = 'Ordinario';
TIPPROCESO[CAMPANA] = 'Campaña';


export const EJENACIONAL = 'Ejecutivo Nacional';
export const EJEESTATAL = 'Ejecutivo Estatal';
export const DIRESTATAL = 'Directivo Estatal';
export const TIPCOMITE = [EJENACIONAL, EJEESTATAL, DIRESTATAL];


export const AMBLOCAL = 'Local';
export const AMBFEDERAL = 'Federal';
export const TIPAMBITO = [AMBLOCAL, AMBFEDERAL];

export const NOTENTIDADES = {
  CR1: 'Circunscripción 1',
  CR2: 'Circunscripción 2',
  CR3: 'Circunscripción 3',
  CR4: 'Circunscripción 4',
  CR5: 'Circunscripción 5',
  NAC: 'Nacional'
};

export const ENTIDADES = merge({
  AGU: 'Aguascalientes',
  BCN: 'Baja California',
  BCS: 'Baja California Sur',
  CAM: 'Campeche',
  CHP: 'Chiapas',
  CHH: 'Chihuahua',
  COA: 'Coahuila',
  COL: 'Colima',
  DIF: 'Ciudad de México',
  DUR: 'Durango',
  GUA: 'Guanajuato',
  GRO: 'Guerrero',
  HID: 'Hidalgo',
  JAL: 'Jalisco',
  MEX: 'Estado de México',
  MIC: 'Michoacán',
  MOR: 'Morelos',
  NAY: 'Nayarit',
  NLE: 'Nuevo León',
  OAX: 'Oaxaca',
  PUE: 'Puebla',
  QUE: 'Queretaro',
  ROO: 'Quintana Roo',
  SLP: 'San Luis Potosí',
  SIN: 'Sinaloa',
  SON: 'Sonora',
  TAB: 'Tabasco',
  TAM: 'Tamaulipas',
  TLA: 'Tlaxcala',
  VER: 'Veracrúz',
  YUC: 'Yucatán',
  ZAC: 'Zacatecas'
},NOTENTIDADES);
