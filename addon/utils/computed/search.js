// utils/computed/search.js
import {computed} from '@ember/object';
import { A } from "@ember/array"

export default function search(dependentKey, propertyKey, searchQueryKey, returnEmptyArray) {
  returnEmptyArray = (typeof returnEmptyArray === "undefined") ? false : returnEmptyArray;
  return computed("" + dependentKey + ".@each." + propertyKey, searchQueryKey, function() {
    var items, query;
    if (returnEmptyArray && !this.get(searchQueryKey)) {
      return A([]);
    }

    query = this.get(searchQueryKey) || '';
    query = query.toLowerCase();
    items = this.get(dependentKey) || A([]);

    return items.filter(function(item) {
      if (item.get(propertyKey)) {
        return item.get(propertyKey).toLowerCase().indexOf(query) !== -1;
      }
    });
  });
}
