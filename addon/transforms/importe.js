import DS from 'ember-data';
import {Decimal} from 'decimal.js';

export default DS.Transform.extend({
  deserialize(serialized) {
    return new Decimal(serialized);
  },

  serialize(deserialized) {
    return deserialized.toNumber();
  }
});
