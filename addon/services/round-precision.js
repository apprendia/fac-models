import Service from '@ember/service';
import Mixin from '@ember/object/mixin';
import Decimal from 'npm:decimal.js-light';

let defaults = {
	decimals: 2,
	precision: 24
}

export default Service.extend(Mixin.create(defaults), {
	/**
	 * @override: ember lifecycle
	 */
	init(...params) {
		this._super(...params);
		this.reset();
	},


	reset() {
		this.setProperties(defaults);
		Decimal.set({ precision: this.get('precision') });
	}
});
