import Elastic from './elasticsearch';

export default Elastic.extend({
  /**
   * @override: ember lifecycle
   */
  init(...params) {
    this._super(...params);
    this.set('defaultOptions', {
      index: 'sat3_3',
      _source: true,
    });
  },

  getCatalogoName(type, id) {
    let cache = this.get('cache') || this.set('cache', {});
    if (cache[`${type}-${id}`]) {
      return cache[`${type}-${id}`];
    } else {
      return cache[`${type}-${id}`] = this.getSource({
        type: type,
        id: id,
        _source: ['name']
      })
    }
  }
});
