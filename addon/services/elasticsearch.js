import { A } from '@ember/array';
import { isPresent } from '@ember/utils';
import { Promise as EmberPromise } from 'rsvp';
import { copy } from '@ember/object/internals';
import { merge } from '@ember/polyfills';
import { get } from '@ember/object';
import Service from '@ember/service';
import DS from 'ember-data';
import config from 'ember-cli-facturacion-logic/mixins/config/environment';
// import elasticsearch from 'npm:elasticsearch';

export default Service.extend({
	client: null,
	init() {
		this._super(...arguments);

		this.set('defaultOptions', {});
		if (get(config, 'elasticsearch')) {
			let client = new window.elasticsearch.Client(
				copy(get(config, 'elasticsearch'), true)
			);
			this.set('client', client);
			return client;
		}
	},

	mergeDefaultOptions(options) {
		return merge(copy(this.get('defaultOptions')), options || {});
	},

	rawSearch(opts) {
		return new EmberPromise((resolve, reject)=>{
			return this.get('client').search(opts, function(err, res) {
				if(isPresent(err)){ return reject(err); }
				return resolve(res);
			});
		});
	},

	search(options) {
		options = this.mergeDefaultOptions(options);

		return DS.PromiseArray.create({
			promise: this.rawSearch(options).then((resp) => {
				let hits = resp.hits.hits;
				return hits.mapBy('_source');
			}).catch((/*err*/) => {
				return [];
			})
		});
	},

	getDocument(options) {
		options = this.mergeDefaultOptions(options);
		try {
			return DS.PromiseObject.create({
				promise: this.get('client').get(options).then((resp) => {
					return get(resp, '_source');
				}, (err) => {
					console.log(err);
					return null;
				})
			});
		} catch (err) {
			console.log(err);
			return null;
		}
	},

	getDocuments(options) {
		options = this.mergeDefaultOptions(options);
		let promise = new EmberPromise((resolve, reject) => {
			return this.get('client').mget(options).then((data)=>{
				let source = A(get(data, 'docs') || []);
				resolve(source.mapBy ? source.mapBy('_source') : []);
			}).catch(reject);
		});

		try {
			return DS.PromiseArray.create({
				promise: promise.then((resp) => {
					return resp;
				}).catch((err) => {
					console.log(err);
					return null;
				})
			});
		} catch (err) {
			console.log(err);
			return null;
		}
	},

	getSource(options){
		options = this.mergeDefaultOptions(options);
		try {
			return DS.PromiseObject.create({
				promise: this.get('client').getSource(options).then((resp) => {
					return resp;
				}, (err) => {
					debugger
					console.log(err);
					return null;
				})
			});
		} catch (err) {
			console.log(err);
			return null;
		}
	}
});
