import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import Service, { inject as service } from '@ember/service';
import DS from 'ember-data';
import jwtDecode from 'npm:jwt-decode';
import {reject} from 'rsvp';

export default Service.extend({
  session: service(),
  store: service(),

  tokenDecoded: null,

  raw: alias('session.currentUser'),
  id: alias('model.id'),

  model: computed('session.currentUser.uid', function() {
    let uid = this.get('session.currentUser.uid');

    if (uid) {
      return DS.PromiseObject.create({
        promise: this.get('store').findRecord('user', uid).then((user) => {
          if(user){
            return this.get('raw').getToken(false).then((token)=>{
              this.set('tokenDecoded', jwtDecode(token));
              return user;
            });
          }
        }).catch(() => {

          return null;
        })
      });
    }
    return null;
  }),
  /**
   * @override: ember lifecycle
   */
  init(...params) {
    this._super(...params);
    this.get('session.currentUser.uid');
    this.get('model');
  }
});
