import { helper } from '@ember/component/helper';
import INEConstants from 'ember-cli-facturacion-logic/utils/catalogos/complemento-ine';

export function disableEntidadByAmbito(params/*, hash*/) {
	let ambito = params[0], key = params[1];
  return ambito == INEConstants.AMBLOCAL && Object.keys(INEConstants.NOTENTIDADES).includes(key) ? true : null
}

export default helper(disableEntidadByAmbito);
