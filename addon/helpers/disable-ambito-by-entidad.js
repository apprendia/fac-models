import { helper } from '@ember/component/helper';
import INEConstants from 'ember-cli-facturacion-logic/utils/catalogos/complemento-ine';

export function disableAmbitoByEntidad(params/*, hash*/) {
  let claveEntidad = params[0], option = params[1];
  return option == INEConstants.AMBLOCAL && Object.keys(INEConstants.NOTENTIDADES).includes(claveEntidad) ? true : null
}

export default helper(disableAmbitoByEntidad);
