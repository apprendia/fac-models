import { helper } from '@ember/component/helper';

export function pluralize(params) {
	let count = params[0],
			singular = params[1],
			plural = params[2] || singular.pluralize();

	return `${count} ${count != 1 ? plural : singular}`;
}

export default helper(pluralize);
