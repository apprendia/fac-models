import originalFormatMoney from 'accounting/helpers/format-money';
import { helper } from '@ember/component/helper';
import Decimal from 'npm:decimal.js-light';

export default helper(function (params, options) {
	let newParams = [],
		opts = {},
		value;

	Object.assign(opts, options);
	Object.assign(newParams, params);

	if (params[0]) {
		value = Decimal(params[0] || 0);
		newParams[0] = value.toNumber();
		opts.precision = Math.max(2, Math.min(value.decimalPlaces(), 6));
		return originalFormatMoney.compute(newParams, opts);
	} else {
		return '';
	}
});
