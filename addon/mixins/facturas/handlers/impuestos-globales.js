import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import Decimal from 'npm:decimal.js-light';
import { inject as service } from '@ember/service';
// import DS from 'ember-data';
// import {
// 	Flags as ImpuestoFlags,
// 	ComputedProps as ImpuestoComputedProps
// } from 'facturacion/models/impuesto';

export default Mixin.create({
	precision: service('round-precision'),
	// impuestosGlobales: Ember.computed('factura.impuestosGlobales.@each.importeForSuma', 'factura.conceptos.@each.subtotal', function() {
	// 	let baseTotal = this.get('factura.conceptos.content').mapBy('subtotal').reduce((carry, importe) => carry + importe, 0);

	// 	return DS.PromiseArray.create({
	// 		promise: this.get('factura.impuestosGlobales').then((array) => {
	// 			array.forEach((impuesto) => {
	// 				if(impuesto.get('isLocal')){
	// 					impuesto.set('base', this.get('factura.subtotal'));
	// 				}else{
	// 					impuesto.set('base', baseTotal);
	// 				}
	// 			});
	// 			return array;
	// 		})
	// 	});
	// }),

	impuestosGlobales: alias('factura.impuestosGlobales.[]'),

	actions: {
		addImpuestoGlobal() {
			debugger
			let factura = this.get('factura');
			let impuesto = factura.get('impuestosGlobales').createRecord({
				isGlobal: true,
				facturaId: factura.get('id')
			});

			impuesto.reopen({
				base: computed('isLocal', 'precision.decimals', function () {
					if (this.get('isLocal')) {
						return Decimal(factura.get('preTotal')).todp(this.get('precision.decimals')).toNumber(); //this.get('factura.preTotal');
					} else {
						return Decimal(factura.get('subtotal')).todp(this.get('precision.decimals')).minus(factura.get('descuento')).todp(this.get('precision.decimals')).toNumber(); //this.get('factura.subtotal');
					}
				}).meta({ serialize: true })
			});
		},


		removedImpuestoGlobal( impuesto ) {
			debugger
			let factura = this.get('factura');
			factura.get('impuestosGlobales').removeObject(impuesto);
		}

	}
});
