import Mixin from '@ember/object/mixin';
import { inject as service } from '@ember/service';
import {resolve} from 'rsvp';

export default Mixin.create({
	currentUser: service(),

  sendEmail: async function(emails){
  	emails = emails || [];
  	let currentAccount = await this.get('currentUser.model.account');

    if (emails.length > 0) {
      return this.get('emailRequests').createRecord({
        account: currentAccount,
        to: emails
      }).save();
    }

    return resolve();
  }
});
