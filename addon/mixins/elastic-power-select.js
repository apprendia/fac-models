import { isEmpty } from '@ember/utils';
import { debounce } from '@ember/runloop';
import { Promise as EmberPromise } from 'rsvp';
import { inject as service } from '@ember/service';
import Mixin from '@ember/object/mixin';
import DS from 'ember-data';

export default Mixin.create({
	elastic: service('elasticsearch'),
	// renderInPlace: true,
	searchEnabled: true,
	searchField: null,
	staticOptions: false,
	sortField: null,

	_sortOptions(array) {
		let sort = this.get('sort') || this.get('sortField');

		switch (true) {
			case (typeof sort == "function"):
				return array.sort(sort);
			case (typeof sort == "string"):
				return array.sortBy(sort);
			default:
				return array;
		}
	},

	loadStaticOptions: function () {
		if (this.get('staticOptions')) {
			this._loadAll()
				.then((array) => {
					this.set('options', this._sortOptions(array));
				})
		}
	}.on('didInsertElement'),

	setSearchFunction: function () {
		this.set('search', this._search.bind(this));
	}.on('didInsertElement'),

	_search(term) {
		return new EmberPromise((resolve, reject) => {
			debounce(this, '_doSearch', term, resolve, reject, 300);
		});
	},

	defaultQuery(term) {
		if (!isEmpty(term)) {
			let obj = {};
			obj[`${this.get('searchField') || 'query'}`] = `${term}*`;

			return {
				query: {
					query_string: obj
				},
				size: 10000
			};
		}
		return {}
	},

	promiseArrayFunction(promise) {
		return DS.PromiseArray.create({
			promise: promise.then((resp) => {
				let hits = resp.hits.hits,
					array = hits.mapBy('_source');

				return this.set('options', this._sortOptions(array));
			}, ( /*err*/ ) => {
				return [];
			})
		});
	},

	_searchPromise(term) {
		let client = this.get('elastic.client'),
			options = {
				index: this.get('index'),
				type: this.get('type'),
			};

		if (term) {
			if (this.get('query')) {
				options['body'] = this.get('query')(this._sanitize_query(term));
			} else {
				options['body'] = this.get('defaultQuery')
					.call(this, this._sanitize_query(term));
			}
		}

		return this.promiseArrayFunction(client.search(options));
	},

	_loadAll() {
		let client = this.get('elastic.client'),
			options = {
				index: this.get('index'),
				type: this.get('type'),
				body: {
					'size': 10000,
					'query': {
						'match_all': {}
					}
				}
			};

		return this.promiseArrayFunction(client.search(options));
	},


	_doSearch(term, resolve, reject) {
		return this._searchPromise(term)
			.then(resolve, reject);
	},

	_sanitize_query(query) {
		return query
			.replace(/[\*\+\-=~><\"\?^\${}\(\)\:\!\/[\]\\\s]/g, '\\$&') // replace single character special characters
			.replace(/\|\|/g, '\\||') // replace ||
			.replace(/\&\&/g, '\\&&') // replace &&
			.replace(/AND/g, '\\A\\N\\D') // replace AND
			.replace(/OR/g, '\\O\\R') // replace OR
			.replace(/NOT/g, '\\N\\O\\T'); // replace NOT
	}
});
