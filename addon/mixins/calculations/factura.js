import { computed, observer } from '@ember/object';
import { mapBy, sum } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import Decimal from 'npm:decimal.js-light';

export default Mixin.create({
	conceptosTotales: mapBy('conceptos', 'total'),
	totalConceptos: sum('conceptosTotales'),

	impFederalesTotales: mapBy('globalesFederales', 'total'),
	totalImpFederales: sum('impFederalesTotales'),

	impLocalesTotales: mapBy('globalesLocales', 'total'),
	totalImpLocales: sum('impLocalesTotales'),

	totalImpGlobales: computed('totalImpFederales', 'totalImpLocales', function () {
		return Decimal(this.get('totalImpFederales') || 0).add(this.get('totalImpLocales') || 0).todp(2).toNumber();
	}),


	conceptosImportes: mapBy('conceptos', 'importe'),
	subtotal: computed('conceptosImportes.[]', function () {
		return this.get('conceptosImportes').reduce((previousValue, importe) => {
			return Decimal(previousValue).add(importe).todp(2).toNumber();
		}, 0);
	}).meta({
		serialize: true
	}),



	conceptosDescuentos: mapBy('conceptos', 'descuento'),
	descuento: computed('conceptosDescuentos.[]', function () {
		return this.get('conceptosDescuentos').reduce((previousValue, importe) => {
			return Decimal(previousValue).add(importe).todp(2).toNumber();
		}, 0);
	}).meta({
		serialize: true
	}),


	preTotal: computed('totalConceptos', 'totalImpFederales', function () {
		return Decimal(this.get('totalConceptos') || 0).todp(2).add(this.get('totalImpFederales') || 0).todp(2).toNumber();
	}),


	total: computed('preTotal', 'totalImpLocales', function () {
		return Decimal(this.get('preTotal') || 0).todp(2).add(this.get('totalImpLocales') || 0).todp(2).toNumber();
	}).meta({
		serialize: true
	}),

	baseNotifies: observer('subtotal', 'preTotal', function () {
		this.get('impuestosGlobales').then((impuestosGlobales) => {
			impuestosGlobales.invoke('notifyPropertyChange', 'base');
		});
	})

});
