import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default Mixin.create({
	isMoral: computed.not('isFisica'),
	isFisica: computed('rfc', function(){
		return isPresent(`${this.get('rfc')}`.match(/^[A-Z]{4}/g));
	}),
});
