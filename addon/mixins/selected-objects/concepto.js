import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import Mixin from '@ember/object/mixin';
import DS from 'ember-data';

export default Mixin.create({
	elastic: service('elasticsearch'),

	claveSelected: computed('clave', {
		get() {
			if (this.get('clave')) {
				return DS.PromiseObject.create({
					promise: this.get('elastic.client').search({
						index: 'sat3_3',
						type: 'c_ClaveProdServ',
						body: {
							query: {
								match: {
									'clave.keyword': this.get('clave')
								}
							}
						}
					}).then((resp) => {
						return resp.hits.hits.mapBy('_source').get('firstObject');
					}, ( /*err*/ ) => {
						return null;
					})
				})
			}
		}
	}),

	claveUnidadSelected: computed('claveUnidad', {
		get() {
			if (this.get('claveUnidad')) {
				return DS.PromiseObject.create({
					promise: this.get('elastic.client').search({
						index: 'sat3_3',
						type: 'c_ClaveUnidad',
						body: {
							query: {
								match: {
									'clave.keyword': this.get('claveUnidad')
								}
							}
						}
					}).then((resp) => {
						return resp.hits.hits.mapBy('_source').get('firstObject');
					}, ( /*err*/ ) => {
						return null;
					})
				})
			}
		}
	})

});
