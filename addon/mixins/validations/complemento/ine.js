import { validator, buildValidations } from 'ember-cp-validations';
import INEConstants from 'ember-cli-facturacion-logic/utils/catalogos/complemento-ine';
import { equal as eq } from '@ember/object/computed';
import { or, not, and } from 'ember-awesome-macros';

export default buildValidations({
  version: validator('presence', true),

  tipoProceso: validator('presence', {
    presence: true,
    message: 'Se debe escoger un tipo de proceso INE',
  }),

  tipoComite: [
    validator('presence', {
      presence: true,
      dependentKeys: ['model.tipoProceso'],
      message: 'Se debe escoger un tipo de comité para el proceso INE seleccionado',
      disabled: not(eq('model.tipoProceso', INEConstants.ORDINARIO))
    })
  ],

  idContabilidad: [
    validator('presence', {
      presence: true,
      dependentKeys: ['model.tipoComite', 'model.tipoProceso'],
      message: 'Se debe escribir una clave de contabilidad para el proceso INE seleccionado',
      disabled: or(eq('model.tipoComite', INEConstants.EJEESTATAL), not(eq('model.tipoProceso', INEConstants.ORDINARIO)))
    }),
    validator('number',{
      allowString: true,
      integer: true,
      positive: true,
      message: 'Las claves de contabilidad INE deben tener solo números'
    }),
    validator('length',{
      max: 6,
      message: 'Las claves de contabilidad INE son de máximo 6 números'
    })
  ],

  entidades: [
    validator('length', {
      max: 76,
      message: 'El número máximo de entidades para el proceso INE seleccionado es de 76',
    }),
    validator('length', {
      min: 1,
      dependentKeys: ['model.tipoComite', 'model.tipoProceso'],
      message: 'Se debe agregar información de al menos 1 entidad para el proceso INE seleccionado',
      disabled: not(or(and(eq('model.tipoProceso', INEConstants.ORDINARIO), 'model.tipoComite', not(eq('model.tipoComite', INEConstants.EJENACIONAL))), eq('model.tipoProceso', INEConstants.PRECAMPANA), eq('model.tipoProceso', INEConstants.CAMPANA)))
    }),
    validator('hasMany')
  ]

});
