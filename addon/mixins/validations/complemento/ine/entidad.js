import { validator, buildValidations } from 'ember-cp-validations';
import INEConstants from 'ember-cli-facturacion-logic/utils/catalogos/complemento-ine';
import { equal as eq } from '@ember/object/computed';

let validateAmbitoEntidad = function(value, options, model /*, attribute*/ ) {
	let test = !(model.get('ambito') == INEConstants.AMBLOCAL && Object.keys(INEConstants.NOTENTIDADES).contains(model.get('claveEntidad')));

	return test || 'Una de las combinaciones de Entidad y Ámbito no es válida'
}

export default buildValidations({
  claveEntidad: [
    validator('presence', {
      presence: true,
      message: 'Se debe escoger una entidad para el proceso INE seleccionado'
    }),

    validator(validateAmbitoEntidad)
  ],

  ambito: [
    validator('presence', {
      presence: true,
      message: 'Se debe escoger un ámbito para la entidad del proceso INE',
      dependentKeys: ['model.tipoProceso'],
      disabled: eq('model.tipoProceso', INEConstants.ORDINARIO)
    }),

    validator(validateAmbitoEntidad)
  ],

  contabilidades: validator('hasMany')
});
