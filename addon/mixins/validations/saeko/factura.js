import { not } from '@ember/object/computed';
import {
  validator,
  buildValidations
} from 'ember-cp-validations';

export default buildValidations({
  webhookUrl: validator('presence', {
    presence: true,
    disabled: not('model.isForSaeko'),
    message: 'La factura no fue generada desde Saeko, falta: url de notificación'
  })
})
