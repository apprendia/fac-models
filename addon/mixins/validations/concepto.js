import { not } from '@ember/object/computed';
// import Mixin from '@ember/object/mixin';
import {
  validator,
  buildValidations
} from 'ember-cp-validations';

export default buildValidations({
  cantidad: [
    validator('presence', {
      presence: true,
      disabled: not('model.validateCantidad')
    }),
    validator('number', {
      positive: true,
      allowString: true,
      disabled: not('model.validateCantidad')
    }),
  ],

  importe: [
    validator('presence', {
      presence: true,
      disabled: not('model.validateImporte')
    }),
    validator('number', {
      positive: true,
      allowString: true,
      disabled: not('model.validateImporte')
    }),
  ],

  valorUnitario: [
    validator('presence', {
      presence: true,
      disabled: not('model.validateImporte'),
      message: 'Debe agregarse precio unitario del concepto'
    }),
    validator('number', {
      positive: true,
      allowString: true,
      disabled: not('model.validateImporte'),
      message: 'El precio unitario debe ser un número positivo'
    }),
  ],

  descuento: [
    validator('number', {
      positive: true,
      allowString: true,
      allowBlank: true,
      disabled: not('model.validateImporte')
    }),
  ],

  clave: [
    validator('presence', {
      presence: true,
      message: 'Debe elegir la clave que representa al concepto'
    }),
  ],

  claveUnidad: [
    validator('presence', {
      presence: true,
      message: 'Debe elegir la clave que representa la unidad'
    }),
  ],

  descripcion: [
    validator('presence', {
      presence: true,
      disabled: not('model.validateCantidad'),
      message: 'La descripción del concepto es obligatoria'
    }),
  ],

  iedu: [
    validator('belongs-to', {
      disabled: not('model.validateCantidad'),
      message: 'Hay errores en el complemento IEDU'
    })
  ],

  impuestos: [
    validator('has-many')
  ],

  partes: [
    validator('has-many')
  ]
});
