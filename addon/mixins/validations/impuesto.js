import {
	validator,
	buildValidations
} from 'ember-cp-validations';

export default buildValidations({
	tipo: [
		validator('presence', {
			presence: true,
			message: 'Indique el tipo de impuesto'
		}),
	],

	base: [
		validator('presence', {
			presence: true,
			message: 'La base del impuesto no está declarada'
		}),
	],

	impuesto: [
		validator('presence', {
			presence: true,
			message: 'Indique el tipo de impuesto'
		}),
	],

	tipoFactor: [
		validator('presence', {
			presence: true,
			message: 'Indique si el impuesto se calcula por Tasa o Cuota'
		}),
	],

	tasaCuota: [
		validator('presence', {
			presence: true,
			message: 'Indique si el impuesto se calcula por Tasa o Cuota',
			disabled() {
		      let model = this.get('model');

		      let {
		        tipoFactor
		      } = model.getProperties('tipoFactor');

		      return (tipoFactor == "Exento")
		    }

		}),
	],

	importe: [
		validator('presence', {
			presence: true,
			message: 'Se debe indicar el importe del impuesto',
			disabled() {
		      let model = this.get('model');

		      let {
		        tipoFactor
		      } = model.getProperties('tipoFactor');

		      return (tipoFactor == "Exento")
		    }

		}),
	],
});
