import EmberObject from '@ember/object';
import ElasticPowerSelectMixin from 'ember-cli-facturacion-logic/mixins/elastic-power-select';
import { module, test } from 'qunit';

module('Unit | Mixin | elastic-power-select', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ElasticPowerSelectObject = EmberObject.extend(ElasticPowerSelectMixin);
    let subject = ElasticPowerSelectObject.create();
    assert.ok(subject);
  });
});
