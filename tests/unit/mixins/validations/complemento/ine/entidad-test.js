import EmberObject from '@ember/object';
import ValidationsComplementoIneEntidadMixin from 'ember-cli-facturacion-logic/mixins/validations/complemento/ine/entidad';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/complemento/ine/entidad', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsComplementoIneEntidadObject = EmberObject.extend(ValidationsComplementoIneEntidadMixin);
    let subject = ValidationsComplementoIneEntidadObject.create();
    assert.ok(subject);
  });
});
