import EmberObject from '@ember/object';
import ValidationsIeduMixin from 'ember-cli-facturacion-logic/mixins/validations/iedu';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/iedu', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsIeduObject = EmberObject.extend(ValidationsIeduMixin);
    let subject = ValidationsIeduObject.create();
    assert.ok(subject);
  });
});
