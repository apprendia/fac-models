import EmberObject from '@ember/object';
import ValidationsFacturaMixin from 'ember-cli-facturacion-logic/mixins/validations/factura';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/factura', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsFacturaObject = EmberObject.extend(ValidationsFacturaMixin);
    let subject = ValidationsFacturaObject.create();
    assert.ok(subject);
  });
});
