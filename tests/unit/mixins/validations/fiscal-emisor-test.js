import EmberObject from '@ember/object';
import ValidationsFiscalEmisorMixin from 'ember-cli-facturacion-logic/mixins/validations/fiscal-emisor';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/fiscal-emisor', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsFiscalEmisorObject = EmberObject.extend(ValidationsFiscalEmisorMixin);
    let subject = ValidationsFiscalEmisorObject.create();
    assert.ok(subject);
  });
});
