import EmberObject from '@ember/object';
import ValidationsSaekoFacturaMixin from 'ember-cli-facturacion-logic/mixins/validations/saeko/factura';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/saeko/factura', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsSaekoFacturaObject = EmberObject.extend(ValidationsSaekoFacturaMixin);
    let subject = ValidationsSaekoFacturaObject.create();
    assert.ok(subject);
  });
});
