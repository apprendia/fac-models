import EmberObject from '@ember/object';
import ValidationsConceptoMixin from 'ember-cli-facturacion-logic/mixins/validations/concepto';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/concepto', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsConceptoObject = EmberObject.extend(ValidationsConceptoMixin);
    let subject = ValidationsConceptoObject.create();
    assert.ok(subject);
  });
});
