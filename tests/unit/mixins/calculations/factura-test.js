import EmberObject from '@ember/object';
import CalculationsFacturaMixin from 'ember-cli-facturacion-logic/mixins/calculations/factura';
import { module, test } from 'qunit';

module('Unit | Mixin | calculations/factura', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let CalculationsFacturaObject = EmberObject.extend(CalculationsFacturaMixin);
    let subject = CalculationsFacturaObject.create();
    assert.ok(subject);
  });
});
