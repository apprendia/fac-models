import EmberObject from '@ember/object';
import CalculationsConceptoMixin from 'ember-cli-facturacion-logic/mixins/calculations/concepto';
import { module, test } from 'qunit';

module('Unit | Mixin | calculations/concepto', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let CalculationsConceptoObject = EmberObject.extend(CalculationsConceptoMixin);
    let subject = CalculationsConceptoObject.create();
    assert.ok(subject);
  });
});
