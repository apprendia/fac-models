import EmberObject from '@ember/object';
import CalculationsFiscalPersonaMixin from 'ember-cli-facturacion-logic/mixins/calculations/fiscal-persona';
import { module, test } from 'qunit';

module('Unit | Mixin | calculations/fiscal-persona', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let CalculationsFiscalPersonaObject = EmberObject.extend(CalculationsFiscalPersonaMixin);
    let subject = CalculationsFiscalPersonaObject.create();
    assert.ok(subject);
  });
});
